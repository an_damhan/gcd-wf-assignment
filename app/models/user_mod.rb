class UserMod < ActiveRecord::Base
  
  validates_uniqueness_of :user_id, :scope => :mod_id, :message => "You are already enrolled/assigned this module!"
  
  validates :user_id, presence: true
  validates :mod_id, presence: true
  
  validates_length_of :user_id, :minimum => 1
  validates_length_of :mod_id, :minimum => 1
  
  default_scope -> { order(created_at: :desc) }
  
  belongs_to :mod
  belongs_to :user
  
end