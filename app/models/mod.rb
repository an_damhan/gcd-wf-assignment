class Mod < ActiveRecord::Base
    has_many :user_mods
    has_many :users, through: :user_mods
end
