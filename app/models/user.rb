class User < ActiveRecord::Base
    has_many :user_mods
    has_many :mods, through: :user_mods
    
    has_secure_password
    validates :password, presence: true, length: {minimum: 6}
    
    def self.search(name)
        if name.match(/\A[+-]?\d+?(\.\d+)?\Z/) == nil
            name.downcase!
            where('LOWER(name) LIKE ?', "%#{name}%")
        else
            where('id = ?', name.to_f)
        end
    end
    
    def fullname
      "#{name} #{surname}"
    end
end
