class UsersController < ApplicationController
  before_action :logged_in_user, only: [:edit, :update, :show, :index]
  before_action :correct_user,   only: [:edit, :update, :show]
  before_action :lecturer_or_admin, only: [:index]
  
  def index
       @users = User.search(params[:search])
  end
  
  
  def edit
  end
  

  def update
    if @user.update_attributes(user_params)
      flash[:success] = "Profile updated"
      redirect_to @user
    else
      render 'edit'
    end
  end
  
  
  def show
    @user = User.find(params[:id])
    @mods = Mod.all
    @user_mods = @user.user_mods
  end
  
  
  def new
    @user = User.new
  end
  
  
  def create
    @user = User.new(user_params)
    
    if @user.save
      flash[:success] = "User created successfully"
      redirect_to login_url
    else
      render 'new'
    end
  end
  

  private

    def user_params
      params.require(:user).permit(:name, :surname, :email, :role, :password,
                                   :password_confirmation)
    end
    
    # Confirms a logged-in user.
    def logged_in_user
      unless logged_in?
        flash[:danger] = "Please log in."
        redirect_to login_url
      end
    end
    
    # Confirms a lecturer or admin user.
    def lecturer_or_admin
      unless is_lecturer? || is_admin?
        flash[:danger] = "You must be a lecturer or administrator to use the search functionality"
        redirect_to user_path(:id => current_user.id)
      end
    end

    # Confirms the correct user.
    def correct_user
      @user = User.find(params[:id])
        redirect_to user_path(:id => current_user.id) unless current_user?(@user)
    end
end
