class UserModController < ApplicationController
  
  before_action :logged_in_user, only: [:create, :destroy, :update]
  before_action :lecturer, only: [:update]
  before_action :student_or_admin, only: [:create]
  
  def new
  end

  def show
  end

  def delete
  end
  
  def update
    
    @grade = params[:user_mod][:grade].to_f
    @id = params[:id].to_f
    puts "Updated Grade = #{@grade} for UserMod with id = #{@id}"
    
    if(current_time > grading_deadline)
      flash[:danger] = "You cannot update grades after the grading deadline (#{grading_deadline})"
      redirect_to :back
    elsif @grade<0 || @grade>100
      flash[:danger] = "Please assign a mark between 0 and 100"
      redirect_to :back
    elsif UserMod.update(@id, grade: @grade)
      flash[:success] = "Grade Update Successful"
      redirect_to :back
    else
      flash[:danger] = "Grade Update Failed"
      redirect_to :back
    end
    
  end
  
  def destroy
    if(current_time < withdraw_deadline)
      UserMod.find(params[:id]).destroy
      flash[:success] = "Withdrawl successful"
      redirect_to :back
    else
      flash[:danger] = "You cannot withdraw after the withdrawl deadline (#{withdraw_deadline})"
      redirect_to :back
    end
  end
  
  def new
    @user_mod = UserMod.new
  end

  def create
    @user_mod = UserMod.new(user_mod_params)
    
    if is_student? && User.find(@user_mod[:user_id]).user_mods.count >= 5
      flash[:danger] = "You cannot enroll in more than 5 modules"
      redirect_to :back
    elsif is_admin? && User.find(@user_mod[:user_id]).user_mods.count >= 3
      flash[:danger] = "You cannot assign a lecturer to more than 3 modules"
      redirect_to :back
    elsif is_student? && @user_mod.save
      flash[:success] = "Enrollment successful!"
      redirect_to :back
    elsif is_admin? && @user_mod.save
      flash[:success] = "Module assignment successful!"
      redirect_to :back
    else
      flash[:danger] = "Please select a valid module!"
      redirect_to :back
    end
  end
  
  private
    def current_user
        @current_user ||= User.find_by(id: session[:user_id])
    end
  
    def user_mod_params
      params.require(:user_mod).permit(:user_id, :mod_id)
    end

    # Confirms a logged-in user.
    def logged_in_user
      unless logged_in?
        store_location
        flash[:danger] = "Please log in."
        redirect_to login_url
      end
    end
    
    # Confirms a lecturer user.
    def lecturer
      unless is_lecturer?
        flash[:danger] = "You must be a lecturer to assign grades"
        redirect_to user_path(:id => current_user.id)
      end
    end
    
    # Confirms a student or admin user.
    def student_or_admin
      unless is_student? || is_admin?
        flash[:danger] = "Only students or admins can enroll or assign modules"
        redirect_to user_path(:id => current_user.id)
      end
    end
end