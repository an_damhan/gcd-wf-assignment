module SessionsHelper
  
  require 'time'
  def withdraw_deadline
    # module start date not specified in requirements so assuming the below for <startdate + 3 weeks>
    Time.new(2017, 5, 21, 00, 00, 0)
  end
  
  def grading_deadline
    # module grading deadline not specified in requirements so assuming the below
    Time.new(2017, 6, 1, 00, 00, 0)
  end
  
  def current_time
    Time.now
  end

  def signed_in?
    !!current_user
  end

  def is_admin?
    signed_in? ? current_user.role == 'Admin' : false
  end

  def is_lecturer?
    signed_in? ? current_user.role == 'Lecturer' : false
  end

  def is_student?
    signed_in? ? current_user.role == 'Student' : false
  end

  # Logs in the given user.
  def log_in(user)
    session[:user_id] = user.id
  end

  # Returns the current logged-in user (if any).
  def current_user
    @current_user ||= User.find_by(id: session[:user_id])
  end
  
  # Returns true if the user is logged in, false otherwise.
  def logged_in?
    !current_user.nil?
  end
  
  # Logs out the current user.
  def log_out
    session.delete(:user_id)
    @current_user = nil
  end
  
    # Returns true if the given user is the current user.
  def current_user?(user)
    user == current_user
  end
end