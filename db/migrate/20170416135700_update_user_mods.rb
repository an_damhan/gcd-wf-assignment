class UpdateUserMods < ActiveRecord::Migration
  def change
    change_column :user_mods, :mod_id, :integer, null: false
    change_column :user_mods, :user_id, :integer, null: false
  end
end
