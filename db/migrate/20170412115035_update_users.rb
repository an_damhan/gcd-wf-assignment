class UpdateUsers < ActiveRecord::Migration
  def change
    remove_column :users, :surname
    remove_column :users, :age
    remove_column :users, :address
    
    # 1: admin, 2: lecturer, 3: student
    add_column :users, :role, :integer
  end
end
