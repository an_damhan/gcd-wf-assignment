class CreateUserMods < ActiveRecord::Migration
  def change
    create_table :user_mods do |t|
      t.references :mod, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
