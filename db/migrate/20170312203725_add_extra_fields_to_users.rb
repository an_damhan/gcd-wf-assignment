class AddExtraFieldsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :surname, :string
    add_column :users, :age, :string
    add_column :users, :address, :string
  end
end
