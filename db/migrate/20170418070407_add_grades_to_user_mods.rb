class AddGradesToUserMods < ActiveRecord::Migration
  def change
    add_column :user_mods, :grade, :integer
  end
end
