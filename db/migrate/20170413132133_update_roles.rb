class UpdateRoles < ActiveRecord::Migration
  def change
    rename_column :roles, :description, :name
  end
end
