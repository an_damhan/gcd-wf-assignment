# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

User.create(name:'Adam',email:'admin@gmail.com',password:'password',role:'Admin',surname:'Ant')
User.create(name:'Leo',email:'lecturer@gmail.com',password:'password',role:'Lecturer',surname:'Sayer')
User.create(name:'Sam',email:'student@gmail.com',password:'password',role:'Student',surname:'Mendes')

Mod.create(name:'Module1')
Mod.create(name:'Module2')
Mod.create(name:'Module3')
Mod.create(name:'Module4')
Mod.create(name:'Module5')
Mod.create(name:'Module6')